#!/usr/bin/python3

import os
import random
import shutil
import string
import subprocess
import webbrowser

googletest_url = 'https://github.com/google/googletest.git'
basic_dev_url = 'https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file'
default_url = 'https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/basic-dev-prac-env.git'
default_keybind_file = ".vscode/keybindings.json"
default_keybind_dst_path = '.local/share/code-server/User/keybindings.json'
Config_files_path = '.testbank/test-bank/eval_files/'
Keybind_path_lin = '.config/Code/User'
Keybind_path_win = 'AppData\\Roaming\\Code\\User'
Keybind_path_src = Config_files_path + '.vscode/keybindings.json'


def get_git_questions(name: str):
    gitlab_issue_creation_outfile = f'{name}/git_question_1_instructions.md'
    gitlab_issue_creation_url = 'https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/raw/master/test-bank/Git/gitLab_issue_creation/instructions.md'
    git_cmd_line_outfile = f'{name}/git_question_2_instructions.md'
    git_cmd_line_url = 'https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/raw/master/test-bank/Git/git_command_line/instructions.md'
    cmds = [
        f'curl -o {git_cmd_line_outfile} {git_cmd_line_url}',
        f'curl -o {gitlab_issue_creation_outfile} {gitlab_issue_creation_url}'
    ]
    for cmd in cmds:
        subprocess.run(cmd.split(), check=True)


def getConfigFiles(exam_files_dir: str, dst_dir: str):
    '''
    Purpose: This will copy the vscode configuration files and user documentation to the root of the practice
    environment file structure. Additionally, it enables the custom key bindings established in keybindings.json.
    '''
    if os.path.exists(dst_dir):
        shutil.rmtree(dst_dir)
    shutil.copytree(exam_files_dir, dst_dir, ignore=shutil.ignore_patterns("state", "workplaceStorage",
        "machineid", ".gitignore", "globalStorage"))

def generate_password():
    '''
    Generate a 24 character password
    '''
    password_length = 24
    possible_characters = string.ascii_letters + string.digits

    random_character_list = [random.choice(possible_characters) for i in range(password_length)]
    random_password = "".join(random_character_list)
    return random_password

def create_config(full_name:str):
    '''
    The config.yaml file in vscode server is used to specify the type of auth, password, if certs are used,
    and the bind addr. This is used to serve inside the container. Do not change the bind-addr expecting
    the container to serve on a different IP. You will need to use the --ip flag for that.
    '''
    with open(f'{full_name}/config.yaml', 'w', encoding="utf-8") as config_file:
        config_file.write((
            'bind-addr: 0.0.0.0:8080\n'
            'auth: password\n'
            f'password: {generate_password()}\n'
            'cert: true\n'
        ))

def clone_repo(full_name:str, repo_url:str):
    '''
    Simple subprocess run to clone exam_url in a folder with the persons name
    '''
    cmd = f'git clone {repo_url} {full_name}'
    subprocess.run(cmd.split(), check=True)

def main():
    # Ensure all the background repos are available
    if not os.path.exists('googletest'):
        subprocess.call(['git', 'clone', googletest_url])

    if not os.path.exists(".testbank"):
        subprocess.call(['git', 'clone', basic_dev_url, '.testbank'])

    # Probably only want to do this once. Alternatively, we could change this to always copy new files.
    getConfigFiles(Config_files_path, "./practice-exam/")
    subprocess.run(f'chmod 777 practice-exam'.split(), check=True)

    # Create a config file with access credentials
    create_config("practice-exam")

    # Start the environment
    subprocess.run(["docker-compose" , "up", "-d"], check=True)
    # open the user's browser to the to the environment
    webbrowser.open("https://127.0.0.1:8080", new=0, autoraise=True)
    # Populate an exam
    subprocess.run(["python3", "createPracticeTest.py"], check=True)
    
    print("\nUse the login credentials in 'practice-exam/config.yaml' to log in to the practice environment\n")
    print("Access the Practice Environment at: https://127.0.0.1:8080\n")
    print("To generate a new exam:")
    print("\tcreatePracticeTest.py\n")
    print("To shut down the practice environment:")
    print("\tdocker-compose down")

if __name__ == "__main__":
    main()
